package org.dao;

import java.util.List;
import java.util.Set;

import org.entity.Hotel;
import org.entity.HotelCategory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration; 
import org.springframework.stereotype.Repository;

@Repository
public class HotelDAOImpl implements HotelDAO {

	private Integer MaxID = 0;
	private Integer MaxIDCat = 0;
	
	SessionFactory	 sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();

	
	public HotelDAOImpl()
	{
		Session session = sessionFactory.openSession();
		session.beginTransaction();
        Query q = session.createQuery("select max(id) from Hotel");
        MaxID = (Integer) q.uniqueResult(); 
        q = session.createQuery("select max(id) from HotelCategory");
        MaxIDCat = (Integer) q.uniqueResult();
	}
	
	
	public void add(Hotel hotel) {

        Session session = sessionFactory.openSession();
        hotel.setId(MaxID++);
        session.beginTransaction();
        session.save(hotel);
        session.getTransaction().commit();
		
	}

	public void delete(Integer id) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
	    Query q = session.createQuery("DELETE Hotel WHERE id = " + id);
		q.executeUpdate();
		session.getTransaction().commit();
	}


	@SuppressWarnings("unchecked")
	public List<Hotel> findAll() {
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
        Query q = session.createQuery("from Hotel"); 
        List<Hotel> list = q.list();
        session.getTransaction().commit(); 
		return list;
	}

	public void edit(Hotel hotel) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.update(hotel);
		session.getTransaction().commit();
	}

	public List<Hotel> find(String name, String address) {
		  
		Session session = sessionFactory.openSession();
		session.beginTransaction();
        Query q = session.createQuery("from Hotel"); 
        List<Hotel> list = q.list();
        session.getTransaction().commit(); 
		return list;
	}

	public void addCategory(String hotelCat) {
        Session session = sessionFactory.openSession();
        HotelCategory  hotelCategory = new HotelCategory(MaxIDCat++,hotelCat);
        session.beginTransaction();
        session.save(hotelCategory);
        session.getTransaction().commit();
	}

	public void editCategory(String update, String source) {
		Session session = sessionFactory.openSession();
		session.beginTransaction(); 
        Query q = session.createQuery("from HotelCategory where name = '"+ source +"'"); 
        HotelCategory category =(HotelCategory) q.uniqueResult();
        category.setName(update);
        session.getTransaction().commit();
        session = sessionFactory.openSession();
        session.beginTransaction();
        session.update(category);
		session.getTransaction().commit();
	}



	public List<String> findAllCategories() {
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
        Query q = session.createQuery("select name from HotelCategory");

        List<String> list = q.list();
        session.getTransaction().commit();
  
		return list;
	}
 
	public void deleteCategory(Set<String> data) {
		Session session = sessionFactory.openSession(); 
		for(String category : data)
		{
		session.beginTransaction();
	    Query q = session.createQuery("DELETE HotelCategory WHERE name = '" + category + "'");
		q.executeUpdate();
		session.getTransaction().commit(); 
		}
	}
     public Integer getMaxID()
     {
    	 return MaxID;
     }
     
	}

