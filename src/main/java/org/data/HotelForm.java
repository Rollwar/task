package org.data;
  
import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToIntegerConverter; 
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect; 
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

import org.entity.Hotel;
import org.service.HotelServiceImpl;
import org.task3.MyUI;

public class HotelForm extends FormLayout {

    private TextField name = new TextField("Hotel's name");
    private TextField address = new TextField("Address");
    private TextField rating = new TextField("Rating");
    private TextField link = new TextField("Link");
    private NativeSelect<String> category = new NativeSelect<>("Category");
    private DateField operatesFrom = new DateField("operatesFrom");
    private Button save = new Button("Save");
    private Button edit = new Button("Update");
    private TextField area = new TextField("Description");
    private HotelServiceImpl service;
    private Hotel hotel;
    private MyUI myUI;
    private Binder<Hotel> binder = new Binder<>(Hotel.class);

    
    public HotelForm(MyUI myUI, HotelServiceImpl service) {
    	
    	category.setItems(service.listCategory());
    	
        this.myUI = myUI;
        this.service = service;
        setSizeUndefined();
        HorizontalLayout buttons = new HorizontalLayout(save,edit);
        name.setDescription("Field for putting hotel's name");
        address.setDescription("Field for putting address");
        rating.setDescription("Field for putting rating");
        link.setDescription("Field for putting url");
        category.setDescription("Selector for choosing caregory");
        operatesFrom.setDescription("Selector for choosing date");
        area.setDescription("Field for putting description");
       
        addComponents(name, category, rating, address, operatesFrom,link, area,buttons);
         
        refresh();
        save.setStyleName(ValoTheme.BUTTON_PRIMARY);	
        save.setClickShortcut(KeyCode.ENTER);  
        
        binder.forField(name)
        .asRequired("This field must have a title")
        .bind(Hotel::getName,Hotel::setName);
        binder.forField(address)
        .asRequired("This field must have a title")
        .bind(Hotel::getAddress,Hotel::setAddress);
        binder.forField(link)
        .asRequired("This field must have a title")
        .bind(Hotel::getUrl,Hotel::setUrl);
        binder.forField(area) 
        .bind(Hotel::getFeedback,Hotel::setFeedback);
     binder.forField(category)
     .asRequired("This field must have a title")
      .bind(Hotel::getCategory,Hotel::setCategory);
        binder.forField(rating)
        .asRequired("This field must have a title")
        .withConverter(new StringToIntegerConverter("Must enter a number"))
        .withValidator(rating -> rating >= 0 && rating <= 5,"Rating must be between 0 and 5")
        .bind(Hotel::getRating,Hotel::setRating);
        binder.forField(operatesFrom) 
        .asRequired("This field must have a title")
        .withConverter(new LocalDataToLongConverter())
        .withValidator(operatesFrom -> operatesFrom > 0,"Incorrect date")
       .bind(Hotel::getOperatesFrom,Hotel::setOperatesFrom);
           
        
        name.addValueChangeListener(e -> {
        	if(binder.isValid())
        		save.setEnabled(true); 
        		else save.setEnabled(false);
        	});
        address.addValueChangeListener(e -> {
        	if(binder.isValid()) 
        		save.setEnabled(true);
        	else save.setEnabled(false);
        	});
        rating.addValueChangeListener(e -> {
        	if(binder.isValid()) save.setEnabled(true);
        	else save.setEnabled(false);
        	});
        link.addValueChangeListener(e -> {
        	if(binder.isValid()) 
        		save.setEnabled(true);
        	else save.setEnabled(false);
        	});
        
        
        save.addClickListener(e -> 
        {
        	if(binder.isValid())
        	{
        	save.setEnabled(true);
        	this.save();
        	}
        });
        
        edit.addClickListener(e -> 
        {
        	if(binder.isValid())
        	{
        	edit.setEnabled(true);
        	this.edit();
        	}
        });
        
        
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel; 
        binder.setBean(hotel);   
        setVisible(true);
        name.selectAll();  
    }
    
    public void switchMode(boolean isUpdate)
    {
    	if(isUpdate)
    	{
    		save.setVisible(false);
    		edit.setVisible(true);
    	}
    	else
    	{
    		save.setVisible(true);
    		edit.setVisible(false);
    	}
    }
    
    public void refresh()
    {
    	category.setItems(service.listCategory());     
    }

    private void delete() {	
        service.removeHotel(hotel.getId());
        myUI.updateTable();
        setVisible(false);
    }

    private void save() { 
        service.addHotel(hotel);
        myUI.updateTable();
        setVisible(false);
    }
    
    private void edit() {
        service.updateHotel(hotel);
        myUI.updateTable();
        setVisible(false);
    }

}