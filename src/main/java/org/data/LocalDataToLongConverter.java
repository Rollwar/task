package org.data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.temporal.ChronoField;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;

public class LocalDataToLongConverter implements Converter<LocalDate,Long>,Serializable {

	@Override
	public Result<Long> convertToModel(LocalDate value, ValueContext context) {
        if (value == null) {
            return Result.error("Cannot cast");
        } else {
            return  Result.ok(value.getLong(ChronoField.EPOCH_DAY));
        }
	}

	@Override
	public LocalDate convertToPresentation(Long value, ValueContext context) {
        if (value == null) {
            return null;
        } else {
            return LocalDate.ofEpochDay(value);
        }
	}

}
