package org.task3; 

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.Binder;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PopupView;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.themes.ValoTheme;
 
import org.entity.Hotel;
import org.service.HotelServiceImpl;
import org.data.HotelCategoryForm;
import org.data.HotelForm; 

@Theme("mytheme")
public class MyUI extends UI {
 
	private HotelServiceImpl service = new HotelServiceImpl(); 
	private Grid<Hotel> grid = new Grid<>(Hotel.class);	
    private TextField filterName = new TextField();
    private HotelForm form = new HotelForm(this,service);
    private HotelCategoryForm category = new HotelCategoryForm(this,service);
    private MenuBar barmenu = new MenuBar();
    private Label selectedMenu = new Label("Hotel Label");
    private Set<Hotel> selectedHotels = new HashSet<Hotel>();
    
    
    @Override
    protected void init(VaadinRequest vaadinRequest) {
    	
    	Window subWindow = new Window("Bult update");
    	NativeSelect<String> select = new NativeSelect<>("Choose field");
    	select.setItems("name","address","operatesFrom","rating","url");
        TextField bultField = new TextField("Enter new value");
    	Button sumbitChange = new Button("Update"); 
    	VerticalLayout subContent = new VerticalLayout();
    	
        subWindow.setContent(subContent);

        subContent.addComponents(select,bultField,sumbitChange);
    	subWindow.setModal(true);
    	subWindow.setResizable(false);
    	subWindow.setDraggable(false);
        subWindow.center();
        
        
        
        final VerticalLayout layout = new VerticalLayout();
        filterName.setPlaceholder("filter by name...");
        filterName.addValueChangeListener(e -> search(filterName));
        filterName.setValueChangeMode(ValueChangeMode.LAZY);


        Button clearFilterTextBtn = new Button(FontAwesome.TIMES);
        clearFilterTextBtn.setDescription("Clear the current filter");
        clearFilterTextBtn.addClickListener(e -> filterName.clear());
        
        CssLayout filtering = new CssLayout();
        filtering.addComponents(filterName, clearFilterTextBtn);
        filtering.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);

        Button addHotelBtn = new Button("Add new hotel");
        addHotelBtn.addClickListener(e -> {
            grid.asMultiSelect().clear();
            form.switchMode(false);
         form.setHotel(new Hotel(service.getID(), "", "", new Integer(0),new Long(0), "", "", ""));
        });

        
        
        
        
        Button editHotelBtn = new Button("Edit hotel");
        editHotelBtn.addClickListener(e ->
        {
            form.switchMode(true);
            form.setHotel(selectedHotels.iterator().next());
            grid.asMultiSelect().clear();
        });
        
        
        Button deleteHotelBtn = new Button("Delete hotel");
        deleteHotelBtn.addClickListener(e ->
        {
        	for(Hotel hotel: selectedHotels)
        	{
        		service.removeHotel(hotel.getId());
        	}
        	grid.asMultiSelect().clear();
        	updateTable();
        });
        
        Button BulkUpdateBtn = new Button("Bulk update");
        BulkUpdateBtn.addClickListener(e ->
        { 

        	addWindow(subWindow);
        	
        });
        
        

        
        sumbitChange.addClickListener(e ->
        { 
        	String field = select.getValue();
             for(Hotel hotel : selectedHotels)
             {
            	 
            	 switch(field)
            	 {
            		 case "name": hotel.setName(bultField.getValue()); break;
            		 case "address": hotel.setAddress(bultField.getValue());break;
            		 case "operatesFrom": hotel.setOperatesFrom(Long.parseLong(bultField.getValue())); break;
            		 case "rating":  hotel.setRating(Integer.parseInt(bultField.getValue())); break;
            		 case "url": hotel.setUrl(bultField.getValue()); break;
            	 }
            	 
            	 service.updateHotel(hotel);
            	 subWindow.close();
             }
             Notification.show(selectedHotels.size() + " items updated");
             grid.asMultiSelect().clear();
             updateTable();
             
        });
        
        HorizontalLayout toolbar1 = new HorizontalLayout(barmenu,selectedMenu);
        HorizontalLayout toolbar2 = new HorizontalLayout(filtering,addHotelBtn, editHotelBtn,BulkUpdateBtn,deleteHotelBtn);
        grid.setColumns("id","name", "category","rating","address","operatesFrom");
        
        grid.addColumn(hotel ->
        "<a href='" + hotel.getUrl() + "' target='_blank'>info</a>",
        new HtmlRenderer()).setCaption("URL");
 
        HorizontalLayout main = new HorizontalLayout(grid,form);
        form.setVisible(false);
        main.setSizeFull();
        grid.setSizeFull();
        main.setExpandRatio(grid, 1);

        HorizontalLayout same = new HorizontalLayout(category);
        
        
        layout.addComponents(toolbar1, toolbar2,main,same);
        same.setVisible(false);
        updateTable();

        setContent(layout);

       form.setVisible(false);
       editHotelBtn.setVisible(false);
       deleteHotelBtn.setVisible(false);
       BulkUpdateBtn.setVisible(false);
       
       grid.setSelectionMode(SelectionMode.MULTI);

       grid.addSelectionListener(event -> {
    	   selectedHotels = event.getAllSelectedItems(); 
           
           switch(selectedHotels.size())
           {
           case 0: editHotelBtn.setVisible(false);
                   deleteHotelBtn.setVisible(false);
                   BulkUpdateBtn.setVisible(false); 
                   break;
           case 1: editHotelBtn.setVisible(true);
                   deleteHotelBtn.setVisible(true);
                   BulkUpdateBtn.setVisible(false);
                   break;
           default :
        	       editHotelBtn.setVisible(false);
                   deleteHotelBtn.setVisible(true); 
                   BulkUpdateBtn.setVisible(true);
                   break;
           }
       });
      
        MenuBar.Command mycommand = new MenuBar.Command() {
            MenuItem previous = null;

            public void menuSelected(MenuItem selectedItem) {
            	switch(selectedItem.getText())
            	{
            	case "Hotel List":    
            		toolbar2.setVisible(true);
            		main.setVisible(true);
             		same.setVisible(false);
            		selectedMenu.setValue("Hotels");
            		updateTable();
            		break;
            	case "Hotel Categories": 
            		main.setVisible(false);
            		toolbar2.setVisible(false);
             		same.setVisible(true);
            		selectedMenu.setValue("Categories");
                    break;
            	}
        
            }
        };
        
        MenuItem options = barmenu.addItem("Options", null, null);
        options.addItem("Hotel List",      null, mycommand);
        options.addItem("Hotel Categories", null, mycommand);
    
    }
       

    public void updateTable()
    {
    	List<Hotel>  hotels = service.listHotel();
    	grid.setItems(hotels);
        form.refresh();
    }
    
    public void search(TextField tf)
    {
  //  	List<Hotel>  hotels = service.findByParam(tf.getValue(),tf2.getValue());
 //   	grid.setItems(hotels);
    }
     
    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
